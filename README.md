--> Automatic API creation through "sails generate api "

1) get api
method : GET
http://0.0.0.0:1337/autoapi
response : 
[
  {
    "createdAt": 1594612426497,
    "updatedAt": 1594612426497,
    "id": 1,
    "name": "kirti",
    "access": "admin"
  }
]



2) cteate api
method : POST
http://0.0.0.0:1337/autoapi/2

req payload : {
    "name" : "user11",
    "access" : "public",
}
response :
{
"createdAt": 1594612559500,
"updatedAt": 1594612559500,
"id": 2,
"name": "user1",
"access": "public"
}



3) change api
method : PUT
http://0.0.0.0:1337/autoapi/2

req payload : {
    "name" : "user11",
    "access" : "public",
}
response :
{
"createdAt": 1594612559500,
"updatedAt": 1594612754036,
"id": 2,
"name": "user11",
"access": "public"
}

4) change api, to delete using id
method : DELETE
http://0.0.0.0:1337/autoapi/2

req payload : {
    "name" : "user11",
    "access" : "public",
}
response :
{
"createdAt": 1594612559500,
"updatedAt": 1594612754036,
"id": 2,
"name": "user11",
"access": "public"
}


--> Custom APIs