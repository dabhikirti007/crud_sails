module.exports = function (req, res, next) {
    try{
        if(req.query.check) {
            return next()
        } else {
            return res.status(401).json({
                status: 'failure',
                error: 'unauthorized'
            });
        }
    } catch(e){
        return res.status(401).json({
			status: 'failure',
			error: 'unauthorized'
		});
    }
	
};
