/**
 * AutoApiController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

const async = require('async');
module.exports = {
  customGet : customGet,
  customPost : customPost,
  customPut : customPut,
  customDelete : customDelete,
};

async function customGet(req, res) {
    console.log(`CustomAPI Get`)
    async.auto({
		userDetails: function (cb) {
			AutoApi.find({
				
			}).exec(cb);
		}
	}, function (e, results) {
		if (e) return res.status(500).json({
			status: 'failure',
			message: e.message || 'Something went wrong.'
		});
		if (!results) res.status(404).json({
			status: 'failure',
			message: 'details not found'
		});
		return res.status(200).json(results.userDetails)
	});
    
}
function customPost(req, res) {
    console.log(`CustomAPI Post`)
    async.auto({
		userDetails: function (cb) {
			AutoApi.create({
                "name" : req.body.name,
                "access" : req.body.access,
			}).exec(cb);
		}
	}, function (e, results) {
		if (e) return res.status(500).json({
			status: 'failure',
			message: e.message || 'Something went wrong.'
		});
		if (!results) res.status(404).json({
			status: 'failure',
			message: 'details not found'
		});
		return res.status(200).json(results.userDetails)
	});

}
function customPut(req, res) {
    console.log(`CustomAPI Put`)
    async.auto({
		userDetails: function (cb) {
			AutoApi.update({
				id: req.body.id
			}, {
				name : req.body.name
			}).exec(cb);
		}
	}, function (e, results) {
		if (e) return res.status(500).json({
			status: 'failure',
			message: e.message || 'Something went wrong.'
		});
		if (!results) res.status(404).json({
			status: 'failure',
			message: 'details not found'
		});
		return res.status(200).json(results.userDetails)
	});
}
function customDelete(req, res) {
    console.log(`CustomAPI Delete`)
    async.auto({
		userDetails: function (cb) {
			AutoApi.destroy({
				id : req.body.id
			}).exec(cb);
		}
	}, function (e, results) {
		if (e) return res.status(500).json({
			status: 'failure',
			message: e.message || 'Something went wrong.'
		});
		if (!results) res.status(404).json({
			status: 'failure',
			message: 'details not found'
		});
		return res.status(200).json(results.userDetails)
	});
}


